package maestro.tipsoverlay;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/**
 * Created by Artyom.Borovsky on 21.10.2015.
 */
public class LineBackground extends Drawable {

    private Paint mPaint;

    public LineBackground(int lineWidth, int lineColor) {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStrokeWidth(lineWidth);
        mPaint.setColor(lineColor);
    }

    @Override
    public void draw(Canvas canvas) {
        Rect bound = getBounds();
        int w = bound.width();
        int h = bound.height();

        canvas.drawLine(0, h, w, 0, mPaint);
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter cf) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }
}
