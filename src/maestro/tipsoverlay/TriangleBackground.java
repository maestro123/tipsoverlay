package maestro.tipsoverlay;

import android.graphics.*;
import android.graphics.drawable.Drawable;

public class TriangleBackground extends Drawable {

    private Paint mPaint;
    private float mTopMargin;

    public TriangleBackground(float topMargin, int color) {
        mPaint = new Paint();
        mPaint.setColor(color);
        mPaint.setAntiAlias(true);
        mTopMargin = topMargin;
    }

    @Override
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();

        int w = bounds.width();
        int h = bounds.height();

        Path path = new Path();
        path.moveTo(0, w * mTopMargin);
        path.lineTo(w, 0);
        path.lineTo(w, h);
        path.lineTo(0, h);
        path.close();

        canvas.drawPath(path, mPaint);
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter cf) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }
}