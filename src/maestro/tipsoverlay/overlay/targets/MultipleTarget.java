package maestro.tipsoverlay.overlay.targets;

import android.graphics.Canvas;
import android.graphics.Rect;
import maestro.tipsoverlay.overlay.OverlayView;

import java.util.ArrayList;

/**
 * Created by Artyom.Borovsky on 18.11.2015.
 */
public class MultipleTarget extends OverlayTarget {

    private ArrayList<OverlayTarget> mTargets = new ArrayList<OverlayTarget>();

    @Override
    public Rect getBounds() {
        Rect bounds = new Rect();
        for (OverlayTarget target : mTargets) {
            boolean done = bounds.intersect(target.getBounds());
        }
        return bounds;
    }

    @Override
    public void applyStyle(int styleResource) {
        for (OverlayTarget target : mTargets) {
            target.applyStyle(styleResource);
        }
    }

    @Override
    public void draw(OverlayView overlayView, Canvas canvas) {
        for (OverlayTarget target : mTargets) {
            target.draw(overlayView, canvas);
        }
    }

    @Override
    public boolean hit(int x, int y) {
        for (OverlayTarget target : mTargets) {
            if (target.hit(x, y)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<OverlayTarget> getTargets() {
        return mTargets;
    }

}
