package maestro.tipsoverlay.overlay.targets;

import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;

/**
 * Created by Artyom.Borovsky on 10.11.2015.
 */
public class CircleTarget extends ViewTarget {

    @Override
    public Path getPath(Rect bounds) {
        Path path = new Path();
        path.addCircle(bounds.centerX(), bounds.centerY(), Math.max(bounds.width(), bounds.height()) / 2, Path.Direction.CW);
        return path;
    }

    @Override
    public Path getErasePath(Rect bounds) {
        return getPath(bounds);
    }

    @Override
    public PointF getStartPoint(Rect bounds) {
        return new PointF(bounds.right, bounds.bottom);
    }

}
