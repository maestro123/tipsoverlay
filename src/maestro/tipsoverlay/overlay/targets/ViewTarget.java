package maestro.tipsoverlay.overlay.targets;

import android.app.Activity;
import android.graphics.*;
import android.view.View;
import maestro.tipsoverlay.overlay.OverlayView;

/**
 * Created by Artyom.Borovsky on 10.11.2015.
 */
public abstract class ViewTarget extends OverlayTarget {

    public static final String TAG = ViewTarget.class.getSimpleName();

    private View mView;
    private Paint mPaint;
    private Paint mErasePaint;
    private int mStyleResource = NOT_SET;
    private float mRadius = 5f;

    public ViewTarget() {
        mErasePaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
        mErasePaint.setColor(Color.TRANSPARENT);
        mErasePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.BLUE);
        mPaint.setStrokeWidth(10);

        initialize();
    }

    public ViewTarget onView(View view) {
        mView = view;
        return this;
    }

    public ViewTarget onView(Activity activity, int id) {
        mView = activity.findViewById(id);
        return this;
    }

    public ViewTarget withStyle(int styleResource) {
        mStyleResource = styleResource;
        applyStyle(styleResource);
        return this;
    }

    @Override
    public void draw(OverlayView overlayView, Canvas canvas) {
        final Rect bounds = getBounds();
        final Path path = getPath(bounds);
        final Path erasePath = getErasePath(bounds);

        canvas.save();

        final Matrix matrix = new Matrix();
        final RectF eraseBounds = new RectF();
        erasePath.computeBounds(eraseBounds, true);
        matrix.setScale(getScale(), getScale(), eraseBounds.centerX(), eraseBounds.centerY());
        erasePath.transform(matrix);

        getErasePaint().setPathEffect(getPathEffect(erasePath, getComposeEffect()));
        canvas.drawPath(erasePath, getErasePaint());

        getPaint().setPathEffect(getPathEffect(path, getComposeEffect()));
        canvas.drawPath(path, getPaint());

        canvas.restore();
    }

    public PathEffect getPathEffect(Path path, PathEffect composeEffect) {
        if (composeEffect != null) {
            return new ComposePathEffect(getPathEffect(path), composeEffect);
        }
        return getPathEffect(path);
    }

    public PathEffect getPathEffect(Path path) {
        final PathMeasure measure = new PathMeasure(path, false);
        final float length = measure.getLength();
        PathEffect effect = new DashPathEffect(new float[]{length, length}, length - length * getAnimationPercents());
        if (mRadius > 0f) {
            effect = new ComposePathEffect(effect, new CornerPathEffect(mRadius));
        }
        return effect;
    }

    @Override
    public Rect getBounds() {
        Rect mRect = new Rect();
        if (mView != null) {
            mView.getGlobalVisibleRect(mRect);
        }
        return mRect;
    }

    @Override
    public void applyStyle(int styleResource) {

    }

    @Override
    public boolean hit(int x, int y) {
        return getBounds().contains(x, y);
    }

    public Paint getErasePaint() {
        return mErasePaint;
    }

    public Paint getPaint() {
        return mPaint;
    }

    public PathEffect getComposeEffect() {
        return null;
    }

    public void initialize() {
    }

    public abstract Path getPath(Rect bounds);

    public abstract Path getErasePath(Rect bounds);

    public abstract PointF getStartPoint(Rect bounds);

}
