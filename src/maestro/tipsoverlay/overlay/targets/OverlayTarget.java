package maestro.tipsoverlay.overlay.targets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import maestro.tipsoverlay.overlay.OverlayView;

/**
 * Created by Artyom.Borovsky on 21.10.2015.
 */
public abstract class OverlayTarget {

    public static final int NOT_SET = -1;

    private CharSequence mText;
    private int mTextResource = NOT_SET;
    private long mAnimationTime = NOT_SET;
    private float mScale = 1f;
    private float mAnimationPercents = 1f;

    public OverlayTarget withText(CharSequence text) {
        mText = text;
        return this;
    }

    public OverlayTarget withText(int textResource) {
        mTextResource = textResource;
        return this;
    }

    public OverlayTarget animate() {
        mAnimationTime = 250;
        return this;
    }

    public OverlayTarget animate(long time) {
        mAnimationTime = time;
        return this;
    }

    public CharSequence getText(Context context) {
        if (mTextResource != NOT_SET) {
            return context.getString(mTextResource);
        }
        return mText;
    }

    public void setScale(float scale) {
        mScale = scale;
    }

    public void setAnimationPercents(float percents) {
        mAnimationPercents = percents;
    }

    public float getScale() {
        return mScale;
    }

    public float getAnimationPercents() {
        return mAnimationPercents;
    }

    public abstract Rect getBounds();

    public abstract void applyStyle(int styleResource);

    public abstract void draw(OverlayView overlayView, Canvas canvas);

    public abstract boolean hit(int x, int y);

}
