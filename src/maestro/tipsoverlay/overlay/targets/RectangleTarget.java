package maestro.tipsoverlay.overlay.targets;

import android.graphics.*;

/**
 * Created by Artyom.Borovsky on 10.11.2015.
 */
public class RectangleTarget extends ViewTarget {

    @Override
    public Path getPath(Rect bounds) {
        Path path = new Path();
        path.addRect(new RectF(bounds), Path.Direction.CW);
        return path;
    }

    @Override
    public Path getErasePath(Rect bounds) {
        return getPath(bounds);
    }

    @Override
    public PointF getStartPoint(Rect bounds) {
        return new PointF(bounds.right, bounds.bottom);
    }

}
