package maestro.tipsoverlay.overlay.targets;

import android.graphics.CornerPathEffect;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;

/**
 * Created by Artyom.Borovsky on 10.11.2015.
 */
public class StarTarget extends ViewTarget {

    @Override
    public void initialize() {
        getPaint().setPathEffect(new CornerPathEffect(50F));
        getErasePaint().setPathEffect(new CornerPathEffect(50F));
    }

    @Override
    public Path getPath(Rect bounds) {
        final float radius = Math.max(bounds.width(), bounds.height()) / 2;
        return setStar(bounds.centerX(), bounds.centerY(), radius * 2, radius, 5);
    }

    @Override
    public Path getErasePath(Rect bounds) {
        final float radius = Math.max(bounds.width(), bounds.height()) / 2;
        return setStar(bounds.centerX(), bounds.centerY(), radius * 2, radius, 5);
    }

    @Override
    public PointF getStartPoint(Rect bounds) {
        final float radius = Math.max(bounds.width(), bounds.height()) / 2;
        return new PointF(
                (float) (bounds.centerX() + radius * Math.cos(0)),
                (float) (bounds.centerY() + radius * Math.sin(0))
        );
    }

    public Path setStar(float x, float y, float radius, float innerRadius, int numOfPt) {
        double section = 2.0 * Math.PI / numOfPt;
        Path path = new Path();
        path.moveTo(
                (float) (x + radius * Math.cos(0)),
                (float) (y + radius * Math.sin(0)));
        path.lineTo(
                (float) (x + innerRadius * Math.cos(0 + section / 2.0)),
                (float) (y + innerRadius * Math.sin(0 + section / 2.0)));
        for (int i = 1; i < numOfPt; i++) {
            path.lineTo(
                    (float) (x + radius * Math.cos(section * i)),
                    (float) (y + radius * Math.sin(section * i)));
            path.lineTo(
                    (float) (x + innerRadius * Math.cos(section * i + section / 2.0)),
                    (float) (y + innerRadius * Math.sin(section * i + section / 2.0)));
        }
        path.close();
        return path;
    }

}
