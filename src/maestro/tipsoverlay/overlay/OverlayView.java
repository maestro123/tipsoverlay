package maestro.tipsoverlay.overlay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.*;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import maestro.tipsoverlay.R;
import maestro.tipsoverlay.overlay.targets.OverlayTarget;
import maestro.tipsoverlay.overlay.targets.ViewTarget;

import java.util.ArrayList;

/**
 * Created by Artyom.Borovsky on 21.10.2015.
 */
public class OverlayView extends ViewGroup {

    public static final String TAG = OverlayView.class.getSimpleName();

    private static final Interpolator ANIMATION_INTERPOLATOR = new DecelerateInterpolator(1.2f);

    private ArrayList<OverlayTarget> mTargets = new ArrayList<OverlayTarget>();
    private Button mNextButton;
    private Button mPreviousButton;
    private TextView mTextView;

    private float mStrokeWidth;

    private int mTextMargin = 0;
    private int mStrokeColor = Color.BLUE;
    private int mShadowColor = Color.BLUE;
    private int mBackgroundColor = Color.argb((int) (255 * 0.7f), Color.red(Color.BLACK), Color.green(Color.BLACK), Color.blue(Color.BLACK));

    private int mCurrentTargetIndex = NO_ID;

    private boolean isDrawAnimationLine = false;
    private boolean isAnimating;

    public OverlayView(Context context) {
        super(context);
        init(null);
    }

    public OverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public OverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    void init(AttributeSet attrs) {
        setWillNotDraw(false);

        mStrokeWidth = dimension(4);

        if (attrs != null) {
            applyStyle(getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.OverlayView, 0, 0));
        }

        mNextButton = new Button(getContext());
        mNextButton.setText("Next");
        mNextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Next button clicked", Toast.LENGTH_SHORT).show();
            }
        });
        addView(mNextButton);

        mTextView = new TextView(getContext());
        addView(mTextView);

    }

    public OverlayView setTextStyle(int styleResource) {
        removeView(mTextView);
        mTextView = new TextView(new ContextThemeWrapper(getContext(), styleResource));
        addView(mTextView);
        return this;
    }

    public OverlayView setButtonStyle(int styleResource) {
        removeView(mNextButton);
        mNextButton = new Button(new ContextThemeWrapper(getContext(), styleResource));
        mNextButton.setText("Next 2");
        mNextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switchNext();
            }
        });
        addView(mNextButton);
        return this;
    }

    @Override
    public void setBackgroundColor(int color) {
        mBackgroundColor = color;
    }

    public OverlayView setOverlayBackgroundColor(int color) {
        mBackgroundColor = color;
        return this;
    }

    public OverlayView setOverlayBackgroundColor(int color, float alpha) {
        mBackgroundColor = Color.argb((int) (255 * alpha), Color.red(color), Color.green(color), Color.blue(color));
        return this;
    }

    private OverlayView setStyle(int styleResource) {
        applyStyle(getContext().getTheme().obtainStyledAttributes(styleResource, R.styleable.OverlayView));
        return this;
    }

    private final void applyStyle(TypedArray array) {
        mStrokeWidth = array.getDimension(R.attr.strokeWidth, dimension(4));
        mStrokeColor = array.getColor(R.attr.strokeColor, mStrokeColor);
        mShadowColor = array.getColor(R.attr.shadowColor, mShadowColor);
        mTextMargin = array.getDimensionPixelSize(R.attr.textMargin, 0);
        isDrawAnimationLine = array.getBoolean(R.attr.drawAnimationLine, true);
    }

    private OverlayTarget mTarget;
    private OverlayTarget mPreviousTarget;

    private boolean drawTarget = false;
    private boolean drawPreviousTarget = false;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(mBackgroundColor);
        if (mTarget != null && drawTarget) {
            mTarget.draw(this, canvas);
        }
        if (mPreviousTarget != null && drawPreviousTarget) {
            mPreviousTarget.draw(this, canvas);
        }
        if (percents != 1f && isDrawAnimationLine && mTarget instanceof ViewTarget && mPreviousTarget instanceof ViewTarget) {
            Log.e(TAG, "draw animation path");
            ViewTarget target = (ViewTarget) mTarget;
            ViewTarget previousTarget = (ViewTarget) mPreviousTarget;
            Path path = new Path();
            PointF tPoint = target.getStartPoint(target.getBounds());
            PointF pPoint = target.getStartPoint(previousTarget.getBounds());
            path.moveTo(pPoint.x, pPoint.y);
            path.lineTo(pPoint.x, pPoint.y);
            path.lineTo(tPoint.x, tPoint.y);

            PathMeasure measure = new PathMeasure(path, false);
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(10);
            paint.setColor(Color.BLUE);
            paint.setAntiAlias(true);
            float length = measure.getLength();
            paint.setPathEffect(new DashPathEffect(new float[]{length, length}, length - length * percents));
            canvas.drawPath(path, paint);

        }
    }

    private float percents = 0f;

    public void switchNext() {
        if (isAnimating) {
            return;
        }
        if (true && mCurrentTargetIndex >= mTargets.size() - 1) {
            mCurrentTargetIndex = NO_ID;
        }
        if (mCurrentTargetIndex < mTargets.size() - 1) {
            mCurrentTargetIndex++;
            if (mCurrentTargetIndex > 0) {
                mPreviousTarget = mTarget;
            } else if (true) {
                mPreviousTarget = mTargets.get(mTargets.size() - 1);
            }
            percents = 0f;
            mTarget = mTargets.get(mCurrentTargetIndex);
            ValueAnimator animator = new ValueAnimator();
            animator.setInterpolator(ANIMATION_INTERPOLATOR);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Float value = (Float) animation.getAnimatedValue();
                    if (value >= 1f) {
                        drawTarget = true;
                        drawPreviousTarget = false;
                        mTarget.setAnimationPercents(1f - value);
                        mTarget.setScale(1f - value);
                    } else {
                        drawPreviousTarget = true;
                        drawTarget = false;
                        if (mPreviousTarget != null) {
                            mPreviousTarget.setAnimationPercents(1f - value);
                            mPreviousTarget.setScale(1f - value);
                        }
                    }
                    percents = animation.getAnimatedFraction();
                    invalidate();
                }
            });
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    isAnimating = true;
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    isAnimating = false;
                }
            });
            animator.setDuration(1000);
            animator.setFloatValues(0f, 2f);
            requestLayout();
            animator.start();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        for (OverlayTarget target : mTargets) {
            if (target.hit((int) ev.getX(), (int) ev.getY())) {
                return ((Activity) getContext()).dispatchTouchEvent(ev);
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.e(TAG, "onTouchEvent");
        return false;
    }

    public OverlayView addTarget(OverlayTarget target) {
        mTargets.add(target);
        return this;
    }

    public final void add(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                WindowManager.LayoutParams params = new WindowManager.LayoutParams();
                params.width = WindowManager.LayoutParams.MATCH_PARENT;
                params.height = WindowManager.LayoutParams.MATCH_PARENT;
                params.flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

                params.dimAmount = 0;
                params.format = PixelFormat.TRANSLUCENT;
                params.windowAnimations = 0;
                activity.getWindowManager().addView(OverlayView.this, params);
                OverlayView.this.switchNext();
            }
        });
    }

    private void dismiss() {
        post(new Runnable() {
            @Override
            public void run() {
                if (getContext() != null) {
                    try {
                        ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).removeView(OverlayView.this);
                    } catch (IllegalArgumentException e) {
                        //ignore
                    }
                }
            }
        });
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            dismiss();
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final Rect bounds = mTargets.get(mCurrentTargetIndex).getBounds();

        int textHeight = mTextView.getMeasuredHeight();
        int textWidth = mTextView.getMeasuredWidth();

        int top = bounds.top;
        int bottom = bounds.bottom;

        if (top - t >= textHeight) {
            mTextView.layout(l, (t + top) / 2 - textHeight / 2, r + textWidth, (t + top) / 2 + textHeight / 2);
        } else {
            mTextView.layout(l, (t + top) / 2 - textHeight / 2, r + textWidth, (t + top) / 2 + textHeight / 2);
        }

        mNextButton.layout(r - mNextButton.getMeasuredWidth(), b - mNextButton.getMeasuredHeight(), r, b);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            if (child == mTextView) {
                CharSequence text = mTargets.get(mCurrentTargetIndex).getText(getContext());
                if (TextUtils.isEmpty(text)) {
                    child.setVisibility(View.GONE);
                } else {
                    ((TextView) child).setText(text);
                    child.setVisibility(View.VISIBLE);
                }
            }
            measureChild(child, widthMeasureSpec, heightMeasureSpec);
        }
    }

    public float getStrokeWidth() {
        return mStrokeWidth;
    }

    public int getStrokeColor() {
        return mStrokeColor;
    }

    public int getShadowColor() {
        return mShadowColor;
    }

    private final float dimension(int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

}
