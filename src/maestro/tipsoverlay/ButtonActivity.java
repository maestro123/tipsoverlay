package maestro.tipsoverlay;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import maestro.tipsoverlay.overlay.OverlayView;
import maestro.tipsoverlay.overlay.targets.CircleTarget;
import maestro.tipsoverlay.overlay.targets.RectangleTarget;
import maestro.tipsoverlay.overlay.targets.StarTarget;

/**
 * Created by Artyom.Borovsky on 21.10.2015.
 */
public class ButtonActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.button_activity);

        findViewById(R.id.test_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ButtonActivity.this, "Test button clicked!", Toast.LENGTH_SHORT).show();
            }
        });

        new OverlayView(this)
                .setOverlayBackgroundColor(Color.BLACK, .7f)
                .setButtonStyle(R.style.TipsButton)
                .setTextStyle(R.style.TipsText)
                .addTarget(new CircleTarget().onView(this, R.id.test_button).withText("This is example text"))
                .addTarget(new CircleTarget().onView(this, R.id.test_button2).withText("This is example text"))
                .addTarget(new CircleTarget().onView(this, R.id.test_button3).withText("This is example text"))
                .addTarget(new CircleTarget().onView(this, R.id.button).withText("This is example text"))
                .addTarget(new CircleTarget().onView(this, R.id.button2).withText("This is example text"))
                .addTarget(new StarTarget().onView(this, R.id.test_button2).withText("This is example text #2"))
                .addTarget(new RectangleTarget().onView(this, R.id.test_button3).withText("This is example text #3"))
                .add(this);

    }
}