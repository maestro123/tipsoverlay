package maestro.tipsoverlay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

public class MyActivity extends Activity {

    private static final long BASE_DURATION = 450;

    TextView title;
    TextView desiredView;

    View iconRow1, iconRow2, yourText, skip, main, topBackground, bottomBackground;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        int color = Color.BLUE;
        topBackground = findViewById(R.id.overlay_top_background);
        topBackground.setBackgroundDrawable(
                new LineBackground((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics()), color));
        bottomBackground = findViewById(R.id.overlay_bottom_background);
        bottomBackground.setBackgroundDrawable(new TriangleBackground(.20f, color));

        title = (TextView) findViewById(R.id.welcome_text);
        desiredView = (TextView) findViewById(R.id.desire_text);

        iconRow1 = findViewById(R.id.icon_row_1);
        iconRow2 = findViewById(R.id.icon_row_2);
        yourText = findViewById(R.id.your_app);
        skip = findViewById(R.id.skip);

        main = findViewById(R.id.main_parent);

        getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (topBackground.getWidth() > 0) {
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            playIn();
                        }
                    }, 50);
                    getWindow().getDecorView().getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });

    }

    void playIn() {
        main.setVisibility(View.VISIBLE);
        AnimatorSet set = new AnimatorSet();
        set.setInterpolator(new DecelerateInterpolator(1.3f));
        Rect rect = new Rect();
        topBackground.getGlobalVisibleRect(rect);

        set.play(ObjectAnimator.ofFloat(topBackground, View.TRANSLATION_Y, -rect.bottom - topBackground.getHeight(), 0).setDuration(BASE_DURATION + 300))
                .with(ObjectAnimator.ofFloat(bottomBackground, View.TRANSLATION_Y, bottomBackground.getHeight(), 0).setDuration(BASE_DURATION))
                .with(ObjectAnimator.ofFloat(title, View.TRANSLATION_Y, title.getHeight() + bottomBackground.getHeight(), 0).setDuration(BASE_DURATION + 100))
                .with(ObjectAnimator.ofFloat(desiredView, View.TRANSLATION_Y, desiredView.getHeight() + bottomBackground.getHeight(), 0).setDuration(BASE_DURATION + 200))
                .with(ObjectAnimator.ofFloat(iconRow1, View.TRANSLATION_Y, iconRow1.getHeight() + bottomBackground.getHeight(), 0).setDuration(BASE_DURATION + 300))
                .with(ObjectAnimator.ofFloat(iconRow2, View.TRANSLATION_Y, iconRow2.getHeight() + bottomBackground.getHeight(), 0).setDuration(BASE_DURATION + 400))
                .with(ObjectAnimator.ofFloat(yourText, View.TRANSLATION_Y, yourText.getHeight() + bottomBackground.getHeight(), 0).setDuration(BASE_DURATION + 500))
                .with(ObjectAnimator.ofFloat(skip, View.TRANSLATION_Y, skip.getHeight() + bottomBackground.getHeight(), 0).setDuration(BASE_DURATION + 600));

        set.start();
    }

    void playOut() {
        main.setVisibility(View.VISIBLE);
        AnimatorSet set = new AnimatorSet();
        set.setInterpolator(new DecelerateInterpolator(1.3f));
        Rect rect = new Rect();
        topBackground.getGlobalVisibleRect(rect);

        set.play(ObjectAnimator.ofFloat(topBackground, View.TRANSLATION_Y, 0, -rect.bottom - topBackground.getHeight()).setDuration(BASE_DURATION + 500))
                .with(ObjectAnimator.ofFloat(bottomBackground, View.TRANSLATION_Y, 0, bottomBackground.getHeight()).setDuration(BASE_DURATION + 800))
                .with(ObjectAnimator.ofFloat(title, View.TRANSLATION_Y, 0, title.getHeight() + bottomBackground.getHeight()).setDuration(BASE_DURATION + 500))
                .with(ObjectAnimator.ofFloat(desiredView, View.TRANSLATION_Y, 0, desiredView.getHeight() + bottomBackground.getHeight()).setDuration(BASE_DURATION + 400))
                .with(ObjectAnimator.ofFloat(iconRow1, View.TRANSLATION_Y, 0, iconRow1.getHeight() + bottomBackground.getHeight()).setDuration(BASE_DURATION + 300))
                .with(ObjectAnimator.ofFloat(iconRow2, View.TRANSLATION_Y, 0, iconRow2.getHeight() + bottomBackground.getHeight()).setDuration(BASE_DURATION + 200))
                .with(ObjectAnimator.ofFloat(yourText, View.TRANSLATION_Y, 0, yourText.getHeight() + bottomBackground.getHeight()).setDuration(BASE_DURATION + 100))
                .with(ObjectAnimator.ofFloat(skip, View.TRANSLATION_Y, 0, skip.getHeight() + bottomBackground.getHeight()).setDuration(BASE_DURATION));

        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Intent intent = new Intent(getBaseContext(), ButtonActivity.class);
                finish();
                startActivity(intent);
            }
        });

        set.start();
    }

    @Override
    public void onBackPressed() {
        playOut();
//        super.onBackPressed();
    }
}
